/**
 * Класс для запуска игры.
 *
 */

public class GameLauncher {
    public static void main(String[] args) {
        GuessGame game =new GuessGame();
        game.startGame();
    }
}
