/**
 * Класс для прощитывания и нахождения верного ответа
 *
 */

public class GuessGame {
    Player p1;
    Player p2;
    Player p3;


    public void startGame() {
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();

        int quessp1 = 0;
        int quessp2 = 0;
        int quessp3 = 0;

        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Я думаю о число между 0 и 9...");

        while (true) {
            System.out.println("Число, которое нужно угадать " + targetNumber);

            p1.quess();
            p2.quess();
            p3.quess();

            quessp1 = p1.number;
            System.out.println("Первый игрок угадывает число " + quessp1);
            quessp2 = p2.number;
            System.out.println("Второй игрок угадывает число " + quessp2);
            quessp3 = p3.number;
            System.out.println("Третий игрок угадывает число " + quessp3);

            if (quessp1 == targetNumber) {
                p1isRight = true;
            }
            if (quessp2 == targetNumber) {
                p2isRight = true;
            }
            if (quessp3 == targetNumber) {
                p3isRight = true;
            }
            if (p1isRight || p2isRight || p3isRight) {
                System.out.println("У нас есть победитель!");
                System.out.println("Игрок один угадал? " + p1isRight);
                System.out.println("Игрок два угадал? " + p2isRight);
                System.out.println("Игрок три угадал? " + p3isRight);
                System.out.println("Конец игры");
                break;
            } else {
                System.out.println("Игрокам дают возможность попробовать еще раз.");
            }


        }
    }
}
